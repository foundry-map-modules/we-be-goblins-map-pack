# We Be Goblins Map Pack

This FoundryVTT module is a collection of maps for the We Be Goblins series of adventures published by Paizo for Pathfinder. All of which are available for free on their [site](https://paizo.com/store/pathfinder/adventures/standalone/freeRPGDay/firstEditionFreeRPG).   
Currently contains maps for
* [We Be Goblins](https://paizo.com/products/btpy8j5w?Pathfinder-Module-We-Be-Goblins)

The module and maps are free, but if you would like to make a donation feel free.  
https://ko-fi.com/skepticrobot  
https://www.buymeacoffee.com/SkepticRobot

# Credit
**Paizo** - Some of these maps use trademarks and/or copyrights owned by Paizo Inc., used under Paizo's [Community Use Policy](https://www.paizo.com/communityuse). We are expressly prohibited from charging you to use or access this content. These maps are not published, endorsed, or specifically approved by Paizo. For more information about Paizo Inc. and Paizo products, visit [paizo.com](https://paizo.com).

**Forgotten Adventures** - maps were made with assets from [Forgotten Adventures](https://www.forgotten-adventures.net/). They are really good.

# Usage

Each adventure has a journal entry with links to the relevant scenes and macros. Scenes have walls and tiles all ready to go.

![Preview](https://gitlab.com/foundry-map-modules/we-be-goblins-map-pack/-/raw/main/preview/preview.webp)
